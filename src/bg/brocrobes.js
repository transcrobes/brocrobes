"use strict";

let username = '';
let password = '';
let baseUrl = '';
let token = '';
let fromLang = '';

const enrichableTags = ['A', 'H1', 'H2', 'H3', 'H4', 'TD', 'TH', 'P', 'FONT', 'DIV', 'SPAN'];

// Helper functions
function parseJwt (token) {
    // TODO: this will apparently not do unicode properly. For the moment we don't care.
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    return JSON.parse(window.atob(base64));
};

function onError(e) {
    console.error(e);
};

function to_enrich(charstr) {
    // TODO: find out why the results are different if these consts are global...
    const zhReg = /[\u4e00-\u9fa5]+/gi;
    const enReg = /[[A-z]+/gi;

    switch (fromLang)
    {
        case 'en':
            return enReg.test(charstr);
        case 'zh-Hans':
            return zhReg.test(charstr);
        default:
            return false;
    }
};

function pageIsUnenriched() {
    return (!(token));
};

function runWithCreds(callback, canRunCallback) {
    if (!(canRunCallback())) {
        alert('Please refresh the page before attempting this action again');
        return;  // TODO: offer to reload from here
    }
    // TODO: think about checking the validity of the token and/or using the refresh
    // rather than just blindly getting another token
    const gettingStoredSettings = browser.storage.local.get();
    gettingStoredSettings.then(
        function(restoredSettings) {
            username = restoredSettings.authCredentials.username;
            password = restoredSettings.authCredentials.password;

            if (restoredSettings.authCredentials.baseUrl.endsWith('/')) {
                baseUrl = restoredSettings.authCredentials.baseUrl;
            } else {
                baseUrl = restoredSettings.authCredentials.baseUrl + '/';
            }

            // TODO: think about using the refresh token
            var xhr = new XMLHttpRequest();
            xhr.open("POST", baseUrl + 'api/token/', true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xhr.responseType = 'json';
            xhr.onreadystatechange = function() { // Call a function when the state changes.
                if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                    token = xhr.response['access']
                    fromLang = parseJwt(token)['lang_pair'].split(':')[0];

                    callback();
                }
            }
            xhr.send("username=" + username + "&password=" + password);
        }, onError);
}

// TODO: FIXME:
// these common extension functions should be shared not copy/pasted!!!
function doCreateElement(elType, elClass, elInnerText, elAttrs, elParent) {
    if (!(elType)) { throw "eltype must be an element name"; };
    const el = document.createElement(elType);
    if (!!(elClass)) {
        el.classList.add(elClass);
    }
    if (!!(elInnerText)) {
        el.textContent = elInnerText;
    }
    if (!!(elAttrs)) {
        for (let attr of elAttrs) {
            el.setAttribute(attr[0], attr[1]);
        }
    }
    if (!!(elParent)) {
        elParent.appendChild(el);
    }

    return el;
}

function printInfos(info, parentDiv) {
    const infoDiv = doCreateElement('div', 'tc-stats', null, null, parentDiv);
    $.each(info, function(i) {
        var e = info[i];
        if (!!(e['metas'])) {
            const infoElem = doCreateElement('div', 'tc-' + e['name'] + 's', null, null, infoDiv);
            doCreateElement('div', 'tc-' + e['name'], e['metas'], null, infoElem);
        } else {
            doCreateElement('div', 'tc-' + e['name'], 'No ' + e['name'] + ' found', null, infoDiv);
        }
        doCreateElement('hr', null, null, null, infoDiv);
    })
}
// TODO: FIXME: end common functions

// Style for elements
const tcrobeEntry = 'padding-left: 6px; position: relative; cursor: pointer; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;';
// FIXME: now we have only one floating popup, this needs to be revisited
const tcrobeDefPopup = 'all: initial; * { all: unset; box-sizing: border-box; } height: 400px; background-color: #555; color: #fff; text-align: center; border-radius: 6px; padding: 3px 0; position: absolute; z-index: 1000; top: 120%; right: 50%; margin-right: -80px; display: none; width: 350px;';
const tcrobeDefContainer = 'text-align: left;';
const tcrobeDefSource = 'margin-left: 6px; padding: 5px 0;';
const tcrobeDefSourceHeader = 'box-sizing: border-box;';
const tcrobeDefSourceName = 'box-sizing: border-box; float: left; text-align: left; width: 50%;';
const tcrobeDefIcons = 'box-sizing: border-box; float: left; text-align: right; width: 50%;';
const tcrobeDefSourcePos = 'margin-left: 12px;';
const tcrobeDefSourcePosDefs = 'margin-left: 18px; padding: 0 0 0 5px;';
const tcrobeDefHeader = 'box-sizing: border-box; display: flex;';
const tcrobeDefPinyin = 'box-sizing: border-box; float: left; width: 20%; padding: 2px;';
const tcrobeDefBest = 'box-sizing: border-box; float: left; width: 60%; padding: 2px;';
const tcrobeDefSentbutton = 'box-sizing: border-box; float: left; width: 50%; padding: 2px;';
// End style for elements

function getToken() {
    return token;
}

var outOfScope = function (xhr) {
    xhr.setRequestHeader ("Authorization", "Bearer " + getToken());
}

function runLate(event, apiVerb, note, addNew) {
    $.ajax({
        type: 'POST',
        cache: false,
        context: $(event.target)[0],
        url: baseUrl + 'notes/' + apiVerb,
        data: JSON.stringify(note),
        datatype: 'json',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Bearer " + token);
        },
        success: function(data) {
            // TODO: Set image to success
            note['Is_Known'] = 1;

            //remove existing defs if we are setting is_known = true
            if (!addNew) {
                const eqDefs = document.getElementsByClassName("tcrobe-def");
                for (var i=0; i<eqDefs.length; i++) {
                    const deft = eqDefs[i];
                    if (deft.dataset.tcrobeDefId == note.Simplified) {
                        deft.parentElement.removeChild(deft);
                    }
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:'); console.log(jqXHR); console.log('textStatus:'); console.log(textStatus); console.log('errorThrown:'); console.log(errorThrown);
        }
    })
}

function populatePopup(levent, popup, ds) {
    levent.stopPropagation();
    // this allows to have the popup on links and if click again then the link will activate
    if (!($(popup).is(":visible"))) {
        levent.preventDefault();
    }
    // place the popup just under the clicked item
    const width = parseInt(popup.style.width,10);
    if (levent.pageX < (width / 2)) { popup.style.left = '0px';
    } else { popup.style.left = (levent.pageX - (width / 2)) + 'px'; }
    popup.style.top = (levent.pageY + 20) + 'px';

    $(popup).show()

    popup.innerHTML = '';

    const defHeader = doCreateElement('div', 'tcrobe-def-header', null, [["style", tcrobeDefHeader]], popup)
    defHeader.appendChild(doCreateElement('div', 'tcrobe-def-pinyin', ds['pinyin'].join(""), [['style', tcrobeDefPinyin]]));
    defHeader.appendChild(doCreateElement('div', 'tcrobe-def-best', !!(ds['best_guess']) ? ds['best_guess']['normalizedTarget'].split(",")[0].split(";")[0] : '', [['style', tcrobeDefBest]]));

    const sentButton = doCreateElement('div', 'tcrobe-def-sentbutton', null, [["style", tcrobeDefSentbutton]], defHeader);
    const sentTrans = $(levent.target).closest('.tcrobe-sent')[0].dataset.sentTrans;

    const popupExtras = doCreateElement('div', 'tcrobe-def-extras', null, null, popup);
    const popupSentence = doCreateElement('div', 'tcrobe-def-sentence', sentTrans, null, popupExtras);
    $(popupExtras).hide();
    $(doCreateElement('img', 'tcrobe-def-sentbutton-img', null, [["src", chrome.runtime.getURL('/img/plus.png')]], sentButton)).click( { pop: popupExtras }, function(event) {
            $(event.data.pop).toggle();
            event.stopPropagation();
        });

    const popupContainer = doCreateElement('div', 'tcrobe-def-container', null, [['style', tcrobeDefContainer]], popup);
    doCreateElement('hr', null, null, null, popupContainer);
    printInfos(ds['stats'], popupContainer);

    function add_better(event) {
        const ds = event.data.ds; const source = event.data.source; const aEntry = event.data.ankrobes_entry; const addNew = event.data.addNew;
        const apiVerb = addNew ? 'add_note_chromecrobes' : 'set_word_known';

        //FIXME: we can have more than one anki entry, and this is going to be problematic...
        let note = (!!(aEntry) && !!(aEntry.length)) ? aEntry[0] : null;

        if (!(note)) {
            note = {"Simplified": ds['word'], "Pinyin": ds['pinyin'].join("")};
            let meaning = '';
            const sourceDef = ds['definitions'][source];
            for (var provider in sourceDef) {
                // TODO: find out how this is possible...
                if (sourceDef[provider].length == 0) { continue; }

                meaning += " (" + provider + "). ";
                const means = []
                for (var pos in sourceDef[provider]) {
                    means.push(sourceDef[provider][pos]['normalizedTarget']);
                }
                meaning += means.join(', ');
            }
            note['Meaning'] = meaning;
        }

        runWithCreds(
            runLate.bind(this, event, apiVerb, note, addNew)
            , function (){ return true; }
        );

        event.stopPropagation();
    };

    const defs = ds['definitions'];
    for (var source in defs) {
        const sources = defs[source];
        // FIXME: this shouldn't be necessary, can remove when the bug putting empty provider+POS definition arrays in source dataset is fixed
        let hasDefs = false;
        for (var pos_def in sources) {
            const actual_defs = sources[pos_def];
            if (actual_defs.length == 0) {
                continue; // how do we get here?????
            } else { hasDefs = true; break; }
        }
        if (!hasDefs) { continue; }

        popupContainer.appendChild(doCreateElement('hr', 'tcrobe-def-hr', null, null));
        const defSource = doCreateElement('div', 'tcrobe-def-source', null, [['style', tcrobeDefSource]], popupContainer);
        const defSourceHeader = doCreateElement('div', 'tcrobe-def-source-header', null, [['style', tcrobeDefSourceHeader]], defSource);
        defSourceHeader.appendChild(doCreateElement('div', 'tcrobe-def-source-name', source, [['style', tcrobeDefSourceName]]));
        const defSourceIcons = doCreateElement('div', 'tcrobe-def-icons', null, [['style', tcrobeDefIcons]], defSourceHeader);

        if (!(ds['ankrobes_entry']) || !(ds['ankrobes_entry'].length)) {
            $(doCreateElement('img', "tcrobe-def-plus", null, [["src", chrome.runtime.getURL('/img/plus.png')], ['style', 'display: inline; width:32px; height:32px; padding:3px;']], defSourceIcons)).click({ ankrobes_entry: ds['ankrobes_entry'], ds: ds, source: source, addNew: true }, add_better);
        }

        // add update button
        $(doCreateElement('img', "tcrobe-def-good", null, [["src", chrome.runtime.getURL('/img/good.png')], ['style', 'display: inline; width:32px; height:32px; padding:3px;']], defSourceIcons)).click({ ankrobes_entry: ds['ankrobes_entry'], ds: ds, source: source, addNew: false }, add_better);

        // FIXME: the following is necessary to avoid fucked up alignment for the first pos header
        // I REALLY hate html...
        defSourceHeader.appendChild( document.createTextNode( '\u00A0' ) );

        for (var pos_def in sources) {
            const actual_defs = sources[pos_def];
            // FIXME: remove when bug adding empty collections of POS defs is fixed.
            if (!(actual_defs.length)) { continue; }

            defSource.appendChild(doCreateElement('div', 'tcrobe-def-source-pos', pos_def, [['style', tcrobeDefSourcePos]]));
            const defSourcePosDefs = doCreateElement('div', 'tcrobe-def-source-pos-defs', null, [['style', tcrobeDefSourcePosDefs]], defSource);
            for (var def in actual_defs) {
                let sep = "";
                if (def > 0) { sep = ", "; }
                defSourcePosDefs.appendChild(doCreateElement('span', 'tcrobe-def-source-pos-def', sep + actual_defs[def]['normalizedTarget'], null));
            }
        }
    }
}

//function runEnrich(event, username, password, baseUrl) {
function runEnrich(event) {

    let known_words = 0;
    let unknown_words = 0;

    const pops = doCreateElement('span', 'tcrobe-def-popup', null, [['style', tcrobeDefPopup]], document.body);
    pops.attributes.id = 'dapopsicle';

    $('body *').contents().filter(function() {
        return (this.nodeType == 3) && this.nodeValue.match(/\S/) && enrichableTags.includes(this.parentElement.nodeName);
    }).each( function( index, element ){
        // Don't send stuff that has no chars to translate. This might happen when there is mixed language on a page
        // TODO: this may get triggered even if it shouldn't - sometimes characters don't get enriched and should
        if (!to_enrich(this.nodeValue)) {
            console.log("Not enriching: " + this.nodeValue);
            return;
        }
        var callingState = new Error(); // capture state/stack when ajax is called
        function myAjax() {
            $.ajax({
                type: 'POST',
                cache: false,
                context: element,
                url: baseUrl + 'enrich/enrich_json',
                data: element.nodeValue,
                datatype: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader ("Authorization", "Bearer " + token);
                },
                error: function(xhr, status, error) {
                    console.log(error);
                },
                success: function(data) {
                    const defEl = element.parentElement;
                    const sents = doCreateElement('span', 'tcrobe-sents', null, null);

                    for (var sindex in data['sentences']) {
                        const s = data['sentences'][sindex];
                        const sent = doCreateElement('span', 'tcrobe-sent', null, null);
                        sent.dataset.sentCleaned = s['cleaned'];
                        sent.dataset.sentTrans = s['translation'];

                        for (var tindex in s['tokens']) {
                            const t = s['tokens'][tindex]; // MUST NOT be var!!!! TODO: find out why!
                            const w = t['word'];
                            if ('ankrobes_entry' in t) {
                                const entry = doCreateElement('span', 'tcrobe-entry', null, [['style', tcrobeEntry]]);
                                entry.dataset.tcrobeEntry = t;

                                const popie = doCreateElement('span', 'tcrobe-def-popup', null, [['style', tcrobeDefPopup]]);
                                entry.appendChild(popie);

                                entry.addEventListener("click", function(event) {
                                    populatePopup(event, pops, t);
                                });

                                entry.appendChild(doCreateElement('span', 'tcrobe-word', t['word'], null));

                                if (!(t['ankrobes_entry']) || !(t['ankrobes_entry'].length) || t['ankrobes_entry'][0]['Is_Known'] == 0) {
                                    const defin = doCreateElement('span', 'tcrobe-def', '(' + t['best_guess']['normalizedTarget'].split(",")[0].split(";")[0] + ')', null);

                                    defin.dataset.tcrobeDef = t['best_guess']['normalizedTarget'].split(",")[0].split(";")[0];
                                    defin.dataset.tcrobeDefId = t['word'];

                                    entry.appendChild(defin);
                                    unknown_words++;
                                    console.log("Document contains " + known_words + " and " + unknown_words + ", or " + (known_words / (known_words + unknown_words)) * 100 + '% known');
                                } else {
                                    known_words++;
                                    console.log("Document contains " + known_words + " and " + unknown_words + ", or " + (known_words / (known_words + unknown_words)) * 100 + '% known');
                                };
                                sent.appendChild(entry);
                            } else {
                                const only_latin = !(to_enrich(w));
                                sent.appendChild(document.createTextNode(only_latin ? " " + w : w));
                            }
                        }
                        sents.appendChild(sent);
                    }
                    defEl.replaceChild(sents, element);
                }
            })};
        myAjax();
    });

    $(document).click(function(event) {
        $(".tcrobe-def-popup").hide();
    });
}

/*
 * TODO: this just makes it harder and isn't really necessary at the moment
function doc_keyUp(e) {
    if ((e.altKey && e.key == 't') || (e.ctrlKey && e.key == 'e')) {
        e.stopPropagation();
        runLoadCreds(e);
    }
}
function stopAltT(e) {
    if ((e.altKey && e.key == 't') || (e.ctrlKey && e.key == 'e')) {
        e.stopPropagation();
    }
}

document.addEventListener('keyup', doc_keyUp, false);
document.addEventListener('keydown', stopAltT, false);
*/

browser.runtime.onMessage.addListener(request => {
    runWithCreds(runEnrich, pageIsUnenriched);
    return Promise.resolve({response: "Sending response"});
});

// the following is a nasty hack to get rid of wikimedia/wikipedia keyboard shortcuts
// I really need to find a proper solution for this...
// see https://en.wikipedia.org/wiki/Wikipedia_talk:WikiProject_User_scripts/Scripts/removeAccessKeys
// for this hack
//$(function () { $("[accesskey]").removeAttr("accesskey").each(function () { this.title = this.title.replace(/ \[alt-.*/, ''); }); });
